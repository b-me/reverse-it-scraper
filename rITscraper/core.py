import requests
import urllib

from rITscraper.parser import ReverseItParser


class Requester(object):

    uri_template = 'https://www.reverse.it/search?query={query}'
    uri_page_template = uri_template + '&page={page}'
    default_headers = {
        'user-agent': (
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'
            ' (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
        )
    }
    
    def __init__(self, config=None):
        self.headers = dict(self.default_headers)
        self.cookies = {}
        if isinstance(config, dict):
            if 'headers' in config:
                self.headers.update(config['headers'])
            if 'cookies' in config:
                self.cookies.update(config['cookies'])

    def get_page(self, query, page_number=None):
        if page_number is None:
            uri = self.uri_template.format(query=urllib.quote_plus(query))
        else:
            uri = self.uri_page_template.format(
                query=urllib.quote_plus(query),
                page=urllib.quote_plus(str(page_number))
            )

        response = requests.get(
            uri,
            headers=self.headers,
            cookies=self.cookies
        )
        return response.content


class ScraperProcessor(object):

    @classmethod
    def process(cls, query, input_file=False, config=None):
        conf_dict = config or {}
        if input_file:
            return cls.process_input_file(query, conf_dict)
        return cls.process_query(query, conf_dict)

    @classmethod
    def process_query(cls, query, config):
        requester = Requester(config.get('requester', None))
        pages = [ReverseItParser(requester.get_page(query))]
        page_count = pages[0].extract_number_of_pages()

        for number in range(2, page_count + 1):
            pages.append(ReverseItParser(requester.get_page(query, number)))

        entries = []
        for page in pages:
            entries.extend(page.extract_entries(config))

        return entries

    @classmethod
    def process_input_file(cls, file_name, config):
        result = []
        with open(file_name, 'r') as f:
            parser = ReverseItParser(f.read())
            result.extend(parser.extract_entries(config))
        return result
