import click
import datetime
import json
import traceback

from rITscraper.core import ScraperProcessor
from rITscraper.dumper import JSONDumper
from rITscraper.exceptions import RITException


class DateParam(click.ParamType):
    name = 'date'

    def __repr__(self):
        return 'DATE'

    def convert(self, value, param, ctx):
        if isinstance(value, datetime.date):
            return value
        try:
            return datetime.datetime.strptime(value, '%d.%m.%Y').date()
        except ValueError:
            self.fail(
                '%s is not a valid date in DD.MM.YYYY format' % value,
                param, ctx)


class JSONConfigParam(click.File):
    name = 'filename'

    def convert(self, value, param, ctx):
        if isinstance(value, dict):
            return value

        f = super(JSONConfigParam, self).convert(value, param, ctx)

        try:
            config = json.load(f)
            if 'till_date' in config:
                try:
                    config['till_date'] = datetime.datetime.strptime(
                        config['till_date'], '%d.%m.%Y').date()
                except ValueError:
                    self.fail(
                        'In %s config file "till_date" is not a'
                        ' valid date in DD.MM.YYYY format' % value,
                        param, ctx)
            if 'from_date' in config:
                try:
                    config['from_date'] = datetime.datetime.strptime(
                        config['from_date'], '%d.%m.%Y').date()
                except ValueError:
                    self.fail(
                        'In %s config file "from_date" is not a'
                        ' valid date in DD.MM.YYYY format' % value,
                        param, ctx)
            return config
        except (AttributeError, TypeError, ValueError):
            self.fail(
                '%s is not a valid JSON file' % value, param, ctx)


@click.command()
@click.argument('query')
@click.option('--file-type', type=str, default=None,
              help='String for filtering file types of entries. icontains')
@click.option('--from-date', type=DateParam(), default=None,
              help='Date in format DD.MM.YYYY for filtering entries from.')
@click.option('--till-date', type=DateParam(), default=None,
              help='Date in format DD.MM.YYYY for filtering entries till.')
@click.option('--output', type=str, default='result.json',
              help='Path to result file. Default is: result.json')
@click.option('--config', type=JSONConfigParam('r'), default=None,
              help='Path to config file.')
@click.option('--print', is_flag=True, default=False,
              help=('Flag if program should print to stdout'
                    ' rather than write to output file.'))
@click.option('--input-file', is_flag=True, default=False,
              help=('Flag if program should load file from path QUERY'
                    ' rather then download pages from reverse.it.'))
def cli(query, input_file, config, **kwargs):
    """
    Program returns list of samples from reverse.it.
    The list is taken from search results for QUERY
    """
    print_ = kwargs.pop('print', False)
    if config is not None:
        update = {k: v for k, v in kwargs.items() if v is not None}
        config.update(update)
    else:
        config = kwargs
    try:
        result = ScraperProcessor.process(query, input_file, config)
    except RITException as ex:
        click.echo(str(ex), err=True)
        result = []
    except Exception:
        click.echo(traceback.format_exc(), err=True)
        result = []
    if print_:
        click.echo(JSONDumper.dump_to_string(result))
    else:
        JSONDumper.dump_to_path(result, config['output'])
