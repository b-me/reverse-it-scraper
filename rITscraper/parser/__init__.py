import re

from collections import OrderedDict
from dateutil import parser as dateparser

from rITscraper.exceptions import RITException

from .lexer import Lexer
from .parser import Parser


class Tools(object):

    class EntryDoesNotMatchFilterError(Exception):
        pass

    @staticmethod
    def clean_attr(attr):
        return attr.replace('\\"', '').replace('\"', '')

    @staticmethod
    def extract_link(href):
        link = Tools.clean_attr(href)
        if 'http' not in link:
            return u'https://reverse.it{}'.format(link)
        return link

    @staticmethod
    def extract_text(element):
        text_element = element.find('text')
        if text_element:
            return text_element.value

    @staticmethod
    def extract_timestamp(element):
        ret = OrderedDict()
        text = Tools.extract_text(element)
        if text:
            # Remove () from timestamp string
            datetime = dateparser.parse(re.sub(r'\(|\)', '', text))
            ret['timestamp'] = datetime
        return ret

    @staticmethod
    def extract_input(element):
        ret = OrderedDict()
        name = element.find('b')
        if name:
            v = Tools.extract_text(name)
            if v:
                ret['file_name'] = v.strip()
        file_type = element.find('span', None)
        if file_type:
            v = Tools.extract_text(file_type)
            if v:
                ret['file_type'] = v.strip()
        file_hash = element.find('span', 'lowprio')
        if file_hash:
            v = Tools.extract_text(file_hash)
            if v:
                ret['file_hash'] = v.strip()
        return ret


SAMPLE_ATTR_MAP = {
    'Timestamp': Tools.extract_timestamp,
    'Input': Tools.extract_input
}


class ReverseItParser(object):

    class NoResultsError(RITException):
        pass

    class BadSyntaxError(RITException):
        pass

    def __init__(self, body):
        self.lexer = Lexer()
        self.parser = Parser(self.lexer)
        self._body = body
        self._result = None

    @property
    def result(self):
        if self._result is None:
            self._result = self.parser.parse(self._body)
            if not self._result:
                raise self.BadSyntaxError(
                    'Input have invalid or no HTML syntax')
        return self._result

    def extract_number_of_pages(self):
        pages = 1
        ul = self.result.find('ul', 'pagination')
        if ul is None:
            return pages
        for li in ul.find_all('li'):
            try:
                pages = int(Tools.extract_text(li))
            except ValueError:
                continue
        return pages

    def extract_entries(self, filters=None):
        filters = filters or {}
        entries = []
        submissions_table = self.result.find('table', id='submissions')
        if not submissions_table:
            raise self.NoResultsError('No results for this query')
        submissions_table = submissions_table.find('tbody')
        if not submissions_table:
            raise self.NoResultsError('No results for this query')
        tr_elements = submissions_table.find_all('tr')
        if not tr_elements:
            raise self.NoResultsError('No results for this query')
        while tr_elements:
            tr_element = tr_elements.pop()
            try:
                entries.append(self._build_entry(tr_element, filters))
            except Tools.EntryDoesNotMatchFilterError:
                continue
        if not entries:
            raise self.NoResultsError('No results for this query')
        return entries

    def _build_entry(self, tr_element, filters):
        entry = OrderedDict()
        dt_elements = tr_element.find_all('dt')
        dd_elements = tr_element.find_all('dd')
        for index, element in enumerate(dt_elements):
            entry.update(
                self._process_data(element, dd_elements[index]))
        entry.update(
            self._extract_link(tr_element))
        self._check_filter(
            entry.get('file_type', None),
            filters.get('file_type', None),
            lambda v, f: f.lower() in v.lower())
        self._check_filter(
            entry.get('timestamp', None),
            filters.get('from_date', None),
            lambda v, f: v.date() >= f)
        self._check_filter(
            entry.get('timestamp', None),
            filters.get('till_date', None),
            lambda v, f: v.date() <= f)
        # Convert timestamp
        if 'timestamp' in entry:
            entry['timestamp'] = entry['timestamp'].isoformat()
        return entry

    def _extract_link(self, element):
        td = element.find('td', 'submission-input')
        if not td:
            return {}
        a = td.find('a')
        if not a:
            return {}
        href = a.attrs.get('href')
        if not href:
            return {}
        return {'link': Tools.extract_link(href)}

    def _check_filter(self, value, filter_, filter_func):
        if filter_ is None:
            return None

        if value is None:
            raise Tools.EntryDoesNotMatchFilterError()

        if not filter_func(value, filter_):
            raise Tools.EntryDoesNotMatchFilterError()

    def _process_data(self, dt, dd):
        name = Tools.extract_text(dt)
        if name in SAMPLE_ATTR_MAP:
            return SAMPLE_ATTR_MAP[name](dd)
        return {}
