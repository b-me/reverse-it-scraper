import os

from .lexer import Lexer
from .patched_yacc import yacc
from .structures import Structure, TextStructure


class Parser(object):

    tokens = Lexer.tokens

    ######################
    # Parser configuration
    ######################

    def p_document(self, p):
        """
        document : html_elements
        """
        p[0] = Structure('Document', children=p[1])

    def p_empty(self, p):
        """
        empty :
        """

    def p_html_elements_single(self, p):
        """
        html_elements : html_element
        """
        p[0] = [p[1]]

    def p_html_elements_concat(self, p):
        """
        html_elements : html_elements html_element
        """
        p[1].append(p[2])
        p[0] = p[1]

    def p_html_element(self, p):
        """
        html_element : tag
                     | text_element
        """
        p[0] = p[1]

    def p_text_element(self, p):
        """
        text_element : TEXT
                     | COMMENT
                     | style
                     | script
        """
        p[0] = TextStructure(p[1])

    def p_script(self, p):
        """
        script : SCRIPT_OPEN SCRIPT_CLOSE
               | SCRIPT_OPEN script SCRIPT_CLOSE
               | SCRIPT
               | script SCRIPT
        """
        p[0] = ''.join(p[1:])

    def p_style(self, p):
        """
        style : STYLE_OPEN STYLE_CLOSE
              | STYLE_OPEN STYLE STYLE_CLOSE
              | STYLE
              | style STYLE
        """
        p[0] = ''.join(p[1:])

    def p_tag_opening(self, p):
        """
        tag_opening : TAG_OPEN TAG_NAME attributes TAG_CLOSE
        """
        p[0] = Structure(p[2], p[3])

    def p_tag_closing(self, p):
        """
        tag_closing : TAG_OPEN TAG_SLASH TAG_NAME TAG_CLOSE
        """
        p[0] = p[3]

    def p_tag_with_content(self, p):
        """
        tag : tag_opening html_elements tag_closing repair_damaged_tag_with_content
        """
        p[1].children.extend(p[2])
        p[0] = p[1]

    def p_tag_without_content(self, p):
        """
        tag : tag_opening tag_closing repair_damaged_tag
        """
        p[0] = p[1]

    def p_tag(self, p):
        """
        tag : TAG_OPEN TAG_NAME attributes TAG_SLASH TAG_CLOSE
            | TAG_OPEN EXC_TAG_NAME attributes TAG_CLOSE
            | TAG_OPEN EXC_TAG_NAME attributes TAG_SLASH TAG_CLOSE
        """
        p[0] = Structure(p[2], p[3])

    def p_attributes_single(self, p):
        """
        attributes : attribute
                   | empty
        """
        p[0] = p[1]

    def p_attributes_concat(self, p):
        """
        attributes : attributes attribute
        """
        p[1].update(p[2])
        p[0] = p[1]

    def p_attribute(self, p):
        """
        attribute : TAG_NAME
                  | TAG_NAME ATTR_EQL ATTR_VALUE
        """
        try:
            p[0] = {p[1]: p[3]}
        except IndexError:
            p[0] = {p[1]: True}

    ####################################
    # Tag closing control and reparation
    ####################################

    def is_symbol_type(self, symbol, _type):
        return symbol.type == _type

    is_open_sym = lambda self, s: self.is_symbol_type(s, 'tag_opening')
    is_close_sym = lambda self, s: self.is_symbol_type(s, 'tag_closing')

    def tags_match(self, tag_open, tag_close):
        if not self.is_open_sym(tag_open) or not self.is_close_sym(tag_close):
            raise SyntaxError
        return tag_open.value.name == tag_close.value

    def repair_tags(self, p, name):
        p.lexer.insert_string('</%s>' % name)
        p.parser.lookahead = None

    def p_repair_damaged_tag(self, p):
        """
        repair_damaged_tag :
        """
        if not self.tags_match(p.stack[-2], p.stack[-1]):
            self.repair_tags(p, p.stack[-1].value)

    def p_repair_damaged_tag_with_content(self, p):
        """
        repair_damaged_tag_with_content :
        """
        if not self.tags_match(p.stack[-3], p.stack[-1]):
            self.repair_tags(p, p.stack[-1].value)

    def p_error(self, p):
        pass

    ################
    # Parser methods
    ################

    def __init__(self, lexer=None):
        path = os.path.dirname(os.path.realpath(__file__))
        self._parser = yacc.yacc(module=self,
                                 picklefile=os.path.join(path, 'pickledtab'))
        self._default_lexer = lexer

    def parse(self, *args, **kwargs):
        default_kwargs = {'lexer': self._default_lexer}
        default_kwargs.update(kwargs)
        return self._parser.parse(*args, **default_kwargs)
