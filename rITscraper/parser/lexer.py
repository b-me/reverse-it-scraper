from ply import lex


class Lexer(object):

    ######################
    # Lexer configuration
    ######################

    # States
    states = (
        ('tag', 'exclusive'),
        ('attrvalue', 'exclusive'),
        ('script', 'exclusive'),
        ('style', 'exclusive')
    )

    # List of used tokens
    tokens = (
        'TEXT',
        'COMMENT',
        'EXC_TAG_NAME',
        'TAG_NAME',
        'ATTR_VALUE',
        'ATTR_EQL',
        'TAG_CLOSE',
        'TAG_SLASH',
        'SCRIPT_OPEN',
        'SCRIPT',
        'SCRIPT_CLOSE',
        'STYLE_OPEN',
        'STYLE',
        'STYLE_CLOSE',
        'TAG_OPEN',
    )

    # Ignore space
    t_ANY_ignore = ' '

    # Tokens definitions
    # General

    t_TEXT = r'[^<]+'
    t_COMMENT = r'<!--(.|\n)*?-->|<!(.|\n)*?>'

    @lex.Token(r'<\s*script(.|\n)*?>')
    def t_SCRIPT_OPEN(self, t):
        t.lexer.push_state('script')
        return t

    @lex.Token(r'<\s*style(.|\n)*?>')
    def t_STYLE_OPEN(self, t):
        t.lexer.push_state('style')
        return t

    @lex.Token(r'<(?!(!|\s*script|\s*style))')
    def t_TAG_OPEN(self, t):
        t.lexer.push_state('tag')
        return t

    # Tag

    t_tag_EXC_TAG_NAME = r'br|img|meta|link|input|hr(?!ef)'
    t_tag_TAG_NAME = r'[\w\-_]+'
    t_tag_TAG_SLASH = r'\/'

    @lex.Token(r'\=')
    def t_tag_ATTR_EQL(self, t):
        t.lexer.push_state('attrvalue')
        return t

    @lex.Token(r'>')
    def t_tag_TAG_CLOSE(self, t):
        t.lexer.pop_state()
        return t

    # Attr value

    @lex.Token(r'"(.|\n)*?"|\'(.|\n)*?\'')
    def t_attrvalue_ATTR_VALUE(self, t):
        t.lexer.pop_state()
        return t

    # Script

    t_script_SCRIPT = r'".*?"(?!\\)|\'.*?\'(?!\\)|[^\'"<]+|<'

    @lex.Token(r'<\s*\/\s*script\s*>')
    def t_script_SCRIPT_CLOSE(self, t):
        t.lexer.pop_state()
        return t

    # Style

    t_style_STYLE = r'".*?"(?!\\)|\'.*?\'(?!\\)|[^\'"<]+|<'

    @lex.Token(r'<\s*\/\s*style\s*>')
    def t_style_STYLE_CLOSE(self, t):
        t.lexer.pop_state()
        return t


    # Error handling

    def t_ANY_error(self, t):
        self.errors.append(
            'line {}: Invalid input {}'.format(t.lineno, t.value[0]))
        t.lexer.skip(1)

    ##################
    # Lexer methods
    ##################

    def __init__(self):
        self._lexer = lex.lex(self)
        self.errors = []
        self._fake_tokens = []

    def __iter__(self):
        for token in self._lexer:
            yield token

    def __call__(self, input_string):
        self.input(input_string)
        return self

    def insert_string(self, s):
        lexdata = self._lexer.lexdata
        pos = self._lexer.lexpos
        lexdata = lexdata[:pos] + s + lexdata[pos:]
        self._lexer.lexdata = lexdata
        self._lexer.lexlen = len(lexdata)

    def input(self, input_string):
        self._lexer.input(input_string)
        self.errors = []

    @property
    def token(self):
        try:
            return self._lexer.token
        except AttributeError:
            pass
