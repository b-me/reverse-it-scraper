class Structure(object):

    def __init__(self, name, attrs=None, children=None):
        self.name = name
        self.attrs = attrs
        self.children = children or []

    def __repr__(self):
        return self.name

    def __iter__(self):
        for child in self.children:
            yield child

    def __getitem__(self, item):
        return self.children[item]

    def _check_child(self, child, name, _class, **kwargs):
        if not getattr(child, 'name', None) == name:
            return False

        child_attrs = getattr(child, 'attrs', {}) or {}

        if _class is None and child_attrs.get('class', None) is not None:
            return False
        elif _class is not None and _class not in child_attrs.get('class', ''):
            return False

        for k, v in kwargs.items():
            if k not in child_attrs:
                return False
            if v not in child_attrs.get(k, ''):
                return False

        return True

    def find(self, name, _class='', **kwargs):
        ret = None
        for child in self.children:
            if self._check_child(child, name, _class, **kwargs):
                ret = child
                break
            elif hasattr(child, 'find'):
                ret = child.find(name, _class, **kwargs)
                if ret is not None:
                    break
        return ret

    def find_all(self, name, _class=''):
        ret = []
        for child in self.children:
            if self._check_child(child, name, _class):
                ret.append(child)
            elif hasattr(child, 'find_all'):
                child_list = child.find_all(name, _class)
                if child_list:
                    ret.extend(child_list)
        return ret

    def pop(self):
        return self.children.pop()


class TextStructure(object):

    def __init__(self, value):
        self.name = 'text'
        self.value = value

    def __repr__(self):
        return self.name

    def __iter__(self):
        for child in []:
            yield child

    def __unicode__(self):
        return unicode(self.value)

    def first(self, name):
        pass
