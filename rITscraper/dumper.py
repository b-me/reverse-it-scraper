import json


class JSONDumper(object):

    @classmethod
    def dump_to_string(cls, obj):
        return json.dumps(obj, indent=4)

    @classmethod
    def dump_to_file(cls, obj, _file):
        json.dump(obj, _file, indent=4)

    @classmethod
    def dump_to_path(cls, obj, path):
        with open(path, 'wb') as _file:
            cls.dump_to_file(obj, _file)
