# reverse.it Scraper

author: Marek Bednarski

## Usage

### CLI
```
Usage: ritscraper [OPTIONS] QUERY

  Program returns list of samples from reverse.it. The list is taken from
  search results for QUERY

Options:
  --file-type TEXT   String for filtering file types of entries. icontains
  --from-date DATE   Date in format DD.MM.YYYY for filtering entries from.
  --till-date DATE   Date in format DD.MM.YYYY for filtering entries till.
  --output TEXT      Path to result file. Default is: result.json
  --config FILENAME  Path to config file.
  --print            Flag if program should print to stdout rather than write
                     to output file.
  --input-file       Flag if program should load file from path QUERY rather
                     then download pages from reverse.it.
  --help             Show this message and exit.

```
### Example calls
`ritscraper "some_query"`

Writes to result.json list of samples for query "some_query"

`ritscraper --print "some_query"`

Writes to stdout list of samples for query "some_query"

`ritscraper --output hello.json "some_query"`

Writes to hello.json list of samples for query "some_query"

`ritscraper --output r --file-type "pe32" "some_query"`

Writes to r list of samples with file_type containing (case insensitive) "pe32"
 for query "some_query"