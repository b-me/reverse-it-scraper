from setuptools import setup, find_packages

if __name__ == '__main__':
    setup(
        name='reverse-it-scraper',
        install_requires=[
            'requests==2.8.1',
            'click==5.1',
            'ply==3.8',
            'python-dateutil==2.4.2',
        ],
        version='2.0',
        packages=find_packages(),
        entry_points={
            'console_scripts': [
                'ritscraper = rITscraper.cli:cli'
            ]
        }
    )
